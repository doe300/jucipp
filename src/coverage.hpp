#pragma once

#include "boost/filesystem.hpp"
#include <vector>

namespace Coverage {

  class BranchCoverage {
  public:
    explicit BranchCoverage(unsigned long num_calls, bool fallthrough = false, bool exception = false) noexcept : count(num_calls), is_fallthrough(fallthrough), is_throw(exception) {}

    bool operator<(const BranchCoverage &other) const noexcept;

    unsigned long count;
    bool is_fallthrough;
    bool is_throw;
  };

  class LineCoverage {
  public:
    LineCoverage(unsigned long code_line, unsigned long num_calls, bool skipped_statements = false, std::vector<BranchCoverage> &&jumps = {}) noexcept : line(code_line), count(num_calls), has_unexecuted_statements(skipped_statements), branches(std::move(jumps)) {}

    bool operator<(const LineCoverage &other) const noexcept;

    bool fully_covered() const noexcept;
    bool partially_covered() const noexcept;
    bool not_covered() const noexcept;

    unsigned long line;
    unsigned long count;
    bool has_unexecuted_statements;

    std::vector<BranchCoverage> branches;
  };


  struct FileInfo {
    boost::filesystem::path source_path;
    boost::filesystem::path object_path;
    boost::filesystem::path build_path;
    std::string language_id;
  };

  std::vector<LineCoverage> analyze(const FileInfo &file_info);
} // namespace Coverage
