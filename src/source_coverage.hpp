#pragma once
#include "coverage.hpp"
#include "source_base.hpp"

namespace Source {
  class CoverageView : virtual public Source::BaseView {

    class Renderer : public Gsv::GutterRendererText {
    public:
      Renderer();

      Gdk::RGBA yellow, red, green, transparent;
    };

  public:
    CoverageView(const boost::filesystem::path &file_path, const Glib::RefPtr<Gsv::Language> &language);
    ~CoverageView() override;

    void configure() override;

    std::function<std::vector<Coverage::LineCoverage>()> get_coverage;
    void toggle_coverage();

  private:
    std::unique_ptr<Renderer> renderer;
    std::vector<Coverage::LineCoverage> line_coverage;

    void update_coverage(std::vector<Coverage::LineCoverage> &&coverage);
    void update_gutter(const Coverage::LineCoverage *line_coverage);
    std::string update_tooltip(const Coverage::LineCoverage &line_coverage);

    const Coverage::LineCoverage *find_coverage(int line);
  };
} // namespace Source
